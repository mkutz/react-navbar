import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
	constructor(p) {
		super(p);
		this.state= {
			active: 0
		};
	}
	render() {
		return (
			<div className="App">
				<header className="App-header">
				</header>
					<Navbar 
						parts={[{
							length: 100,
						},{
							length: 200
						}, {
							length: 150
						}]}
						activePart={this.state.active}
						clicked={index => this.setState({active: index})}
					/>
			</div>
		);
	}
}

const Navbar = ({clicked, parts, activePart}) => {
	return <div style={{marginTop: 100, position: 'relative', backgroundColor: 'yellow'}}>
		<div style={{
			backgroundColor: 'green',
			position: 'absolute',
			marginLeft: sumUpTo(activePart-1, parts.map(a=>a.length)) + (parts[activePart].length)/2 - 25/2,
			display: 'inline-block',
			marginTop: 20,
			height: 25,
			width: 25
		}}>

		</div>
		{
			parts.map(({length}, i) => {
				return( 
				<span 
					style={{
						border:"1px solid",
						backgroundColor: 'red',
						width: length,
						display: 'inline-block',
						marginTop: 50
					}}
					onClick={clicked.bind(undefined, i)}
					>
					{ length }
					</span>
				)
			})
		}
		</div>
}

const sumUpTo = (index, arr) => {
	return arr.reduce((sum, curr, i) => i>index ? sum : sum + curr,0)
}

export default App;
